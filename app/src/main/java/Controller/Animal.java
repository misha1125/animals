package Controller;

/**
 * Created by student2 on 09.11.16.
 */

public abstract class Animal {

    Animal(String name,int imageId){
        this.name = name;
        this.imageId = imageId;
    }

    private String name;
    private int imageId;

    public abstract  void eat();

    public String getName() {
        return name;
    }

    public int getImageId() {
        return imageId;
    }
}
