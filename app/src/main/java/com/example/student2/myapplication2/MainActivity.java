package com.example.student2.myapplication2;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import Controller.Animal;
import Controller.Cat;
import Controller.Dog;

public class MainActivity extends AppCompatActivity {

    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout)findViewById(R.id.animal_view);

        ArrayList<Animal> animals = new ArrayList<>();

        animals.add(new Dog("dog",R.drawable.dog));
        animals.add(new Dog("dog2",R.drawable.dog2));
        animals.add(new Cat("cat",R.drawable.cat1));
        animals.add(new Cat("cat2",R.drawable.cat2));

        for (Animal animal : animals){

            ImageView image = new ImageView(this);
            image.setImageResource(animal.getImageId());
            image.setLayoutParams(new ActionBar.LayoutParams(500,500));

            TextView text = new TextView(this);
            text.setText(animal.getName());

            linearLayout.addView(image);
            linearLayout.addView(text);
        }
    }
}
